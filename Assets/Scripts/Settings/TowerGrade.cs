﻿using System;

namespace Assets.Scripts.Settings
{
    [Serializable]
    public class TowerGrade 
    {
        public string prefabName;
        public int cost;
        public float radius;
        public float damage;
        public float bulletSpeed;
        public float cooldown;
    }
}
