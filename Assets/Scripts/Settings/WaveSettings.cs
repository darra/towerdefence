﻿using System.Collections.Generic;
using Assets.Scripts.Game;
using UnityEngine;

namespace Assets.Scripts.Settings
{
    [CreateAssetMenu()]
    public class WaveSettings : ScriptableObject
    {
        public int bunchCount;

        public int minEnemyCountInBunch;
        public int maxEnemyCountInBunch;

        public List<EnemyType> enemyTypes;
        public float cooldownBunchTime;

        public float cooldownWaweTime;

    }
}
