﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Game;
using UnityEngine;

namespace Assets.Scripts.Settings
{
    [Serializable]
    public class TowerSetting : ScriptableObject
    {
        public TowerType type;
        public string towerIcon;
        [SerializeField] public List<TowerGrade> grades;
    }
}
