﻿using Assets.Scripts.Game;
using UnityEngine;

namespace Assets.Scripts.Settings
{
    [CreateAssetMenu()]
    public class EnemySettings : ScriptableObject
    {
        public EnemyType type;
        public int bufferAmount;
        public string prefabName;
        public float HP;
        public float speed;
        public int damage;
        public int damageCooldown;
        public int enemyPrice;
        public float HBOffset;
    }
}
