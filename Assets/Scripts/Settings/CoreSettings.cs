﻿using UnityEngine;

namespace Assets.Scripts.Settings
{
    [CreateAssetMenu()]
    public class CoreSettings : ScriptableObject
    {
        public int CoreHealth;
        public float CoreHBOffsetY;
        public float CoreHBOffsetX;
    }
}
