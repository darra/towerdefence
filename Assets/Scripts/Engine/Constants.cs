﻿namespace Assets.Scripts.Engine
{
    public class Constants
    {
        public static string TowerColliderTag = "towerCollider";
        public static string TowerObjectTag = "towerObject";
        public static string TowerPlaceTag = "towerPlace";
        public static string EnemyTag = "enemy";
        public static string TileTag = "tile";
        public static string BulletTag = "bullet";
        public static string Untagged = "Untagged";
        public static string CoreObject = "core";    
    }
}
