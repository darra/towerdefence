﻿using System.Collections.Generic;

namespace Assets.Scripts.Engine
{
    public class PathFinding
    {
        private GridTile[,] map;
        private const int sizeX = 13;
        private const int sizeY = 15;

        public void FormMapTable(List<GridTile> tiles)
        {
            map = new GridTile [ sizeY, sizeX];

            for (int i = 0; i < tiles.Count; i++)
            {
                GridTile next = tiles[i];
                map[next.rowIndex, next.columnIndex] = next;
            }
        }


        public void FindAllPathes(List<GridTile> tiles, GridTile targetTile)
        {
            FormMapTable(tiles);
            Queue<GridTile> queue = new Queue<GridTile>();

            queue.Enqueue(targetTile);

            while (queue.Count > 0)
            {
                GridTile current = queue.Dequeue();
                List<GridTile> neighbors = GetNeighbors(current);

                for (int i = 0; i < neighbors.Count; i++)
                {
                    if (!neighbors[i].IsObstacle && neighbors[i].NextTile == null && neighbors[i]!= targetTile)
                    {
                        queue.Enqueue(neighbors[i]);
                        neighbors[i].NextTile = current;
                    }
                }
            }
            
        }

        private List<GridTile> GetNeighbors(GridTile tile)
        {
            List<GridTile> neighbors = new List<GridTile>();
            int i = tile.rowIndex;
            int j = tile.columnIndex;

            //down
            if(i-1 >= 0)
                neighbors.Add(map[i-1,j]);
            //up
            if(i+1 < sizeY)
                neighbors.Add(map[i+1, j]);

            //left
            if(j -1 >= 0)
                neighbors.Add(map[i, j-1]);

            //right
            if(j+1 < sizeX)
                neighbors.Add(map[i, j +1]);

            return neighbors;
        }
     }
}
