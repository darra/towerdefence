﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Engine
{
    public class ObjectPool : MonoBehaviour
    {
        private Queue<GameObject> pool;

        private GameObject prefab;
        public int ActiveObjectCount { get; set; }

        public void CreateObjectPool(GameObject prefab, int bufferCount)
        {
            pool = new Queue<GameObject>();
            this.prefab = prefab;
            for (int i = 0; i < bufferCount; i++)
            {
                GameObject go = Instantiate(prefab);
                go.SetActive(false);
                go.AddComponent<PooledObject>().RegisterInPool(this);
                go.transform.SetParent(transform);
                pool.Enqueue(go);
            }
        }

        public GameObject GetObject(Vector3 startPosition, bool resetParent , bool enableImmediately = true)
        {
            GameObject go;
            if (pool.Count > 0)
            {
                go = pool.Dequeue();
            }
            else
            {
                go = Instantiate(prefab);
                go.AddComponent<PooledObject>().RegisterInPool(this);
                go.transform.SetParent(transform); 
            }
            ActiveObjectCount++;
            if (resetParent)
                go.transform.SetParent(null);
            go.transform.position = startPosition;
            go.SetActive(enableImmediately);
            return go;
        }

        public void ReturnObjectToPool(GameObject go)
        {
            go.SetActive(false);
            go.transform.SetParent(transform);
            ActiveObjectCount--;
            pool.Enqueue(go);
        }
    }
}
