﻿using UnityEngine;

namespace Assets.Scripts.Engine
{
    public class PooledObject : MonoBehaviour
    {
        private ObjectPool poolOwn;

        public void RegisterInPool(ObjectPool pool)
        {
            poolOwn = pool;
        }

        public void SelfDestroy()
        {
            poolOwn.ReturnObjectToPool(gameObject);
        }
    }
}
