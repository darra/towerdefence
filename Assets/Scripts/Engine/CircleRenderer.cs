﻿using UnityEngine;

namespace Assets.Scripts.Engine
{
    class CircleRenderer : MonoBehaviour
    {
        private int segments = 50;

        [SerializeField]
        private LineRenderer line;

        public void DrawCircle(float radius)
        {
            line.useWorldSpace = false;
            line.positionCount = segments+1;
            float x;
            float z;

            float angle = 20f;

            for (int i = 0; i < (segments + 1); i++)
            {
                x = Mathf.Sin(Mathf.Deg2Rad * angle) * radius;
                z = Mathf.Cos(Mathf.Deg2Rad * angle) * radius;

                line.SetPosition(i, new Vector3(x, 0, z));

                angle += (360f / segments);
            }
        }

        public void ResetLine()
        {
            line.positionCount = 0;
        }
    }
}
