﻿using UnityEngine;

namespace Assets.Scripts.Engine
{
    public class GridTile : MonoBehaviour
    {
        public int columnIndex;
        public int rowIndex;

        public bool IsObstacle;
        public GridTile NextTile;
       
    }
}
