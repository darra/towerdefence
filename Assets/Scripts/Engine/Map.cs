﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Engine
{
    public class Map : MonoBehaviour
    {
        [SerializeField]
        private List<GridTile> tiles;
        public void AddTile(GridTile tile)
        {
            if(tiles == null)
                tiles = new List<GridTile>();


            tiles.Add(tile);
        }

        public List<GridTile> GetTiles()
        {
            return tiles;
        } 
    }
}
