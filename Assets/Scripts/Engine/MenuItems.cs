﻿#if UNITY_EDITOR

using System;
using Assets.Scripts.Engine;
using UnityEditor;
using UnityEngine;

public class MenuItems : MonoBehaviour
{
    private const int sizeX = 16;
    private const int sizeY = 18;
    //offset-Tiles are not considered as waypoints
    //only for decoration purposes;
    private const int decorationOffset = 3;

    [MenuItem("Tools/Generate Map")]
    public static void GenerateMap()
    {
        GameObject map = new GameObject {name = "Map"};
        GameObject terrain = new GameObject {name = "Terrain"};
        map.transform.position = Vector3.zero;
        Map mapScript = map.AddComponent<Map>();

        for (int i = 0; i < sizeX; i++)
        {
            bool isDecoarationX = /*i < decorationOffset ||*/ i >= sizeX - decorationOffset;
            for (int j = 0; j < sizeY; j++)
            {
                bool isDecoarationY = /*j < decorationOffset || */j >= sizeY - decorationOffset;
                GameObject tile = GameObject.CreatePrimitive(PrimitiveType.Cube);
                tile.transform.localScale = new Vector3(1,0.5f,1);
                tile.transform.position = new Vector3(i, 0, j);
                tile.GetComponent<MeshRenderer>().material = (Material)Resources.Load("ground");
                if (isDecoarationX || isDecoarationY)
                {
                    tile.transform.SetParent(terrain.transform);
                    continue;
                }
                tile.name = String.Format("Tile {0}{1}", i ,j); 
                tile.transform.SetParent(map.transform);
                GridTile tileScript = tile.AddComponent<GridTile>();

                tileScript.columnIndex = i;
                tileScript.rowIndex = j;
                mapScript.AddTile(tileScript);
            }
        }
    }

    [MenuItem("Tools/Make selected objects TowerPlaceholder")]
    public static void MakeTowerPlaceHolder()
    {
        foreach (GameObject go in Selection.gameObjects)
        {
            
            go.AddComponent<TowerPlaceHolder>();
            go.GetComponent<GridTile>().IsObstacle = true;
            go.GetComponent<MeshRenderer>().material = (Material)Resources.Load("towerPlace", typeof(Material));
            go.tag = "towerPlace";
        }
    }
}
#endif