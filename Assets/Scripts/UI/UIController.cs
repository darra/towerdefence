﻿using System.Collections.Generic;
using Assets.Scripts.Engine;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Game
{
    public class UIController : MonoBehaviour
    {
        [SerializeField]
        private GameObject enemyHealthBar;

        [SerializeField]
        private Text textResource;
        [SerializeField]
        private Image waveProgress;
        [SerializeField]
        private GameObject waveMilestone;
        [SerializeField]
        private GameObject endScreen;
        [SerializeField]
        private Text endText;

        [SerializeField] private GameObject ResumeGameBtn;

        [SerializeField]
        private ObjectPool enemyHealthBarPool;

        
        private ResourceManager resourceManager;
        private Vector3 uiOffset;
        private float targetResolutionWidth = 1920;
        private float targetResolutionHeight = 1080;
        private void Awake()
        {
            uiOffset = new Vector3(targetResolutionWidth / 2f, targetResolutionHeight / 2f, 0f);
            resourceManager = GameController.ResourceManager;
            resourceManager.ResourceAmountChanged += OnChangeResource;
            enemyHealthBarPool.CreateObjectPool(enemyHealthBar, 10);
            textResource.text = resourceManager.CurrentResource.ToString();
        }

        public void OnPause()
        {
            GameController.instance.Pause();
            endScreen.SetActive(true);
            ResumeGameBtn.SetActive(true);
            endText.gameObject.SetActive(false);
        }

        public void ResumeGame()
        {
            GameController.instance.Resume();
            endScreen.SetActive(false);
        }

        public void StartGameClick()
        {
            GameController.instance.StartGame();
        }

        public Vector3 GetCanvasPosition(Vector3 worldPosition)
        {
            Vector3 screenPosition = Camera.main.WorldToViewportPoint(worldPosition);
            Vector3 pos = new Vector3(screenPosition.x * targetResolutionWidth , screenPosition.y*targetResolutionHeight,0);
            Vector3 result = pos - uiOffset;
            return result;
        }

        public void OnEndGameClick(bool win)
        {
            endScreen.SetActive(true);
            ResumeGameBtn.SetActive(false);
            endText.gameObject.SetActive(true);
            endText.text = win ? "VICTORY" : "DEFEAT";
        }

        public GameObject GetEnemyHealthBar(Vector3 startPosition)
        {
            return enemyHealthBarPool.GetObject(startPosition, false,false);
        }

        public void InitWawesProgressSlider(float progress, List<float> waveMilestones)
        {
            float sliderLenght = waveProgress.GetComponent<RectTransform>().rect.width;
            for (int i = 0; i < waveMilestones.Count - 1; i++)
            {
                float xPosition = (sliderLenght / progress) * waveMilestones[i];

                GameObject ms = Instantiate(waveMilestone);
                ms.transform.SetParent(waveProgress.transform);
                ms.transform.localScale = Vector3.one;
                ms.transform.localPosition = new Vector3(xPosition - sliderLenght / 2, 0, 0);
            }
        }

        public void UpdatedeWaveSlider(float progress)
        {
            waveProgress.fillAmount = progress;
        }

        private void OnChangeResource()
        {
            int currentResource = resourceManager.CurrentResource;
            textResource.text = currentResource.ToString();
        }
    }
}
