﻿using System.Collections.Generic;
using Assets.Scripts.Engine;
using Assets.Scripts.Game;
using Assets.Scripts.Settings;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class TowerUIManager : MonoBehaviour
    {
        [SerializeField]
        private GameObject gradeButtonPrefab;
        [SerializeField]
        private GameObject towerBuildPopup;
        [SerializeField]
        private TowerMenuButton buttonForBuildTower;

        [SerializeField]
        private ObjectPool gradebuttonPool;
        [SerializeField]
        private CircleRenderer circleRenderer;

        private TowerPlaceHolder selectedTowerPlace;
        private float towerUiOffset = 20f;
        private Tower currentTower;
        private int towerLevel;
        private UIController uiController;
        private PopupType currentOpenPopup;

        public void InitTowerUiManager(List<TowerSetting> towerSettings)
        {
            InitPopup(towerSettings);
            // InitGradeButton();
            gradebuttonPool.CreateObjectPool(gradeButtonPrefab, 4);
            uiController = GameController.UiController;
            GameController.InputController.ClickOnTaggedObject += HideUiComponentMissClick;

            currentOpenPopup = PopupType.none;
        }

        public GameObject GetButton()
        {
            return gradebuttonPool.GetObject(Vector3.zero, false, false);
        }

        public void HideUiComponentMissClick(RaycastHit hit)
        {

            switch (currentOpenPopup)
            {
                case PopupType.build:
                    towerBuildPopup.gameObject.SetActive(false);
                    break;
                case PopupType.none:
                    return;
            }
        }

        private void InitPopup(List<TowerSetting> towerSettings)
        {
            for (int i = 0; i < towerSettings.Count; i++)
            {
                GameObject go = Instantiate(buttonForBuildTower.gameObject);
                TowerMenuButton button = go.GetComponent<TowerMenuButton>();
                button.Init(towerSettings[i], 0);
                button.PointerDown += BuildTower;
                button.PointerExit += ResetPotentialTower;
                button.PointerEnter += ShowPotentialTower;
                go.transform.SetParent(towerBuildPopup.transform);
                go.SetActive(true);
            }
            towerBuildPopup.SetActive(false);
        }


        public void OpenBuildTowerPopup(TowerPlaceHolder place)
        {
            currentOpenPopup = PopupType.build;
            Vector3 positionOnScreen = uiController.GetCanvasPosition(place.transform.position);
            towerBuildPopup.GetComponent<RectTransform>().localPosition = positionOnScreen + Vector3.up * towerUiOffset;
            towerBuildPopup.gameObject.SetActive(true);
            selectedTowerPlace = place;
        }

        public void ShowPotentialTower(TowerSetting selectedTower, int level)
        {
            Vector3 centerPosition = selectedTowerPlace.transform.position + Vector3.up * 0.25f;
            circleRenderer.transform.position = centerPosition;
            circleRenderer.DrawCircle(selectedTower.grades[level].radius);
        }


        public void ResetPotentialTower()
        {
            circleRenderer.ResetLine();
        }

        public void BuildTower(TowerSetting setting)
        {
            currentOpenPopup = PopupType.none;
            GameController.TowerController.BuildTower(setting, selectedTowerPlace.transform);
            towerBuildPopup.SetActive(false);
            selectedTowerPlace.OnBuildTower();
            ResetPotentialTower();
        }


        private void OnDestroy()
        {
            GameController.InputController.ClickOnTaggedObject -= HideUiComponentMissClick;
        }
    }

    enum PopupType
    {
        build,
        upgrade,
        none
    }
}
