﻿using Assets.Scripts.Game;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class HealthInfo : MonoBehaviour
    {
        [SerializeField] private Image healthBar;
        [SerializeField]
        private FallingDownText textDamage;


        public void UpdateHeath(float currenthealth)
        {
          //Debug.LogError(currenthealth);
            healthBar.fillAmount = currenthealth;
        }

        public void ShowDamage(float damage)
        {
          // textDamage.GetComponent<FallingDownText>().StartMovement(damage.ToString());
        }
    }
}
