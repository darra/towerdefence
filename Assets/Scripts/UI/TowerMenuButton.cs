﻿using System;
using Assets.Scripts.Game;
using Assets.Scripts.Settings;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class TowerMenuButton : Selectable
    {
        [SerializeField]
        private Text textOnButton;
        [SerializeField]
        private Image towerIcon;
        private TowerSetting towerSetting;

        public event Action<TowerSetting, int> PointerEnter;
        public event Action PointerExit;
        public event Action<TowerSetting> PointerDown;
        private int price;
        private int level;
        private Action onClickAction;

        public void Init(TowerSetting towerSetting, int level, bool needChangeSprite = true)
        {
            this.towerSetting = towerSetting;
            this.level = level;
            textOnButton.text = towerSetting.grades[level].cost.ToString();

            price = towerSetting.grades[level].cost;
            CheckPrice();

            if (needChangeSprite)
            {
                Sprite image = Resources.Load<Sprite>(towerSetting.towerIcon);

                if (image == null)
                {
                    Debug.LogError("No icon " + towerSetting.towerIcon);
                    return;
                }
                towerIcon.sprite = image;
            }
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            base.OnPointerEnter(eventData);
            if (PointerEnter != null)
                PointerEnter.Invoke(towerSetting, level);
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            base.OnPointerExit(eventData);
            if (PointerExit != null)
                PointerExit.Invoke();
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            base.OnPointerDown(eventData);
            if (!interactable)
                return;
            if (PointerDown != null)
                PointerDown.Invoke(towerSetting);
        }

        private void OnEnable()
        {
            CheckPrice();
        }

        private void CheckPrice()
        {
            if(GameController.ResourceManager == null)
                return;
            if (!GameController.ResourceManager.CanUseResource(price))
            {
                interactable = false;
                textOnButton.color = Color.red;
            }
            else
            {
                interactable = true;
                textOnButton.color = Color.white;
            }
        }

        private void OnDestroy()
        {
            PointerEnter = null;
            PointerDown = null;
            PointerExit = null;
        }
    }
}
