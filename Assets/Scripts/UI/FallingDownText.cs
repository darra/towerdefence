﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Game
{
    public class FallingDownText : Text
    {
        private bool isMoving;
        private float disappearingSpeed = 0.5f;
        private float currentTime;
        private float disappeareTime = 1f;
        public void StartMovement(string val)
        {
            text = val;
            isMoving = true;
        }
        private void Update()
        {
            if (isMoving)
            {
                currentTime += Time.deltaTime;
                color = new Color(color.r, color.g, color.b, color.a - Time.deltaTime * disappearingSpeed);
                //transform.position -= Vector3.down * disappearingSpeed *Time.deltaTime;
                if (currentTime >= disappeareTime)
                {
                    currentTime = 0f;
                    isMoving = false;
                    gameObject.SetActive( false);
                }
            }

            
        }
    }
}
