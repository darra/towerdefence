﻿using System;

namespace Assets.Scripts.Game
{
    public class ResourceManager
    {
        private int currentResources = 20;
        public event Action ResourceAmountChanged;

        public int CurrentResource{get { return currentResources; } }

        public void AddResource(int increnmenter)
        {
            currentResources += increnmenter;
            if(ResourceAmountChanged != null)
                    ResourceAmountChanged.Invoke();
        }

        public bool CanUseResource(int sum)
        {
            return currentResources >= sum;
        }

        public void UseResource(int sum)
        {
            currentResources -= sum;
            if (ResourceAmountChanged != null)
                ResourceAmountChanged.Invoke();
        }

    }
}
