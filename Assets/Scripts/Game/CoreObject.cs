﻿using Assets.Scripts.Settings;
using Assets.Scripts.UI;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class CoreObject : MonoBehaviour
    {
        [SerializeField] private HealthInfo mainInfo;
        [SerializeField] private CoreSettings settings;
        private float currentGemsCount;
        private float startGemsCount;
     

        private void Awake()
        {
            Vector3 positionOnScreen = GameController.UiController.GetCanvasPosition(transform.position);
            mainInfo.GetComponent<RectTransform>().localPosition = positionOnScreen + new Vector3(settings.CoreHBOffsetX, settings.CoreHBOffsetY,0);
            startGemsCount = settings.CoreHealth;
            currentGemsCount = startGemsCount;
            mainInfo.UpdateHeath(1);
        }

        public void GetDamage(int damage)
        {
            currentGemsCount -=damage;
            mainInfo.UpdateHeath(currentGemsCount / startGemsCount);
            if (currentGemsCount <= 0)
            {
                GameController.instance.GameOver(false);
            }
        }
    }
}
