﻿using Assets.Scripts.Engine;
using Assets.Scripts.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Game
{
    public class GameController : MonoBehaviour
    {
        [SerializeField]
        private TowerController towerController;
        [SerializeField]
        private InputController inputController;
        [SerializeField]
        private UIController uiController;
        [SerializeField]
        private Map map;
        [SerializeField]
        private GridTile targetTile;
        [SerializeField]
        private EnemyController enemyController;
        [SerializeField]
        private WavesController waveController;

        public static InputController InputController { get { return instance.inputController; } }
        public static TowerController TowerController { get { return instance.towerController; } }
        public static UIController UiController { get { return instance.uiController; } }
        public static EnemyController EnemyController { get { return instance.enemyController; } }
        public static ResourceManager ResourceManager { get { return instance.resourceManager; } }

        public bool InGame;
        public bool Freez;
        private ResourceManager resourceManager;
        public static GameController instance;


        private void Awake()
        {
            if (instance == null)
                instance = this; 
            PathFinding pathFinding = new PathFinding();

            pathFinding.FindAllPathes(map.GetTiles(), targetTile);
            resourceManager = new ResourceManager();
            InGame = true;
            waveController.StartWaves();
        }

        public void StartGame()
        {
            SceneManager.LoadScene("Main",LoadSceneMode.Single);
        }

        public void Pause()
        {
            InGame = false;
        }

        public void Resume()
        {
            InGame = true;
        }


        public void StartFreez()
        {
            Freez = true;
        }

        public void StopFreeze()
        {
            Freez = false;
        }
        public void GameOver(bool win)
        {
            UiController.OnEndGameClick(win);
            InGame = false;
            //Debug.LogError(!win ? "LOOSE" : "Win");
        }
    }
}
