﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class EnemyAnimator : MonoBehaviour
    {
        [SerializeField] private Animator animator;
        [SerializeField] private string move;
        [SerializeField] private string getDamage;
        [SerializeField] private string die;
        [SerializeField] private string hit;

        private Dictionary<string, float> clipLenght; 
        private void Awake()
        {
            clipLenght = new Dictionary<string, float>();
            for (int i = 0; i < animator.runtimeAnimatorController.animationClips.Length; i++)
            {
                clipLenght.Add(animator.runtimeAnimatorController.animationClips[i].name,
                    animator.runtimeAnimatorController.animationClips[i].length);
            }
        }


        public void PlayMove()
        {
            animator.Play(move);
        }


        public float PlayGetDamage()
        {
            animator.Play(getDamage);
            return clipLenght[getDamage];
        }

        public float PlayDie()
        {
            animator.Play(die);
            return clipLenght[die];
        }

        public float PlayHit()
        {
            animator.Play(hit);
            return clipLenght[hit];
        }
    }
}
