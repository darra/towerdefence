﻿using System;
using Assets.Scripts.Engine;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class Bullet : MonoBehaviour
    {
        private float damage;
        private float speed;
        private bool isBulletIstriggered;

        private Transform targetPosition;
       
        public void InitBullet(Vector3 start, float damage, float speed)
        {
            this.damage = damage;
            this.speed = speed;
            transform.localPosition = new Vector3(0,1,0);
        }

        public void TriggerBullet(Transform target)
        {
            transform.localPosition = new Vector3(0, 2, 0);
            isBulletIstriggered = true;
            targetPosition = target;
        }

        private void OnTriggerEnter(Collider collider)
        {
            if (collider.tag == Constants.EnemyTag)
            {
                BaseEnemy enemy = collider.gameObject.GetComponent<BaseEnemy>();
                if (enemy == null)
                {
                    Debug.LogError("No BaseEnemy script on object");
                    return;
                }

                enemy.GetDamage(damage);
                isBulletIstriggered = false;
                PooledObject bulletPooled = GetComponent<PooledObject>();
                if (bulletPooled == null)
                {
                    Destroy(gameObject);
                }
                else
                {
                    bulletPooled.SelfDestroy();
                }
            }
        }


        public void Update()
        {
            if(!GameController.instance.InGame)
                return;
            if (isBulletIstriggered)
            {
                Vector3 direction = targetPosition.position - transform.position ;
                float way = speed*Time.deltaTime;
                transform.Translate(way*direction.normalized);
                if (Mathf.Approximately(direction.magnitude,Mathf.Epsilon) || !targetPosition.gameObject.activeSelf)
                {
                    isBulletIstriggered = false;
                    PooledObject bulletPooled = GetComponent<PooledObject>();
                    if (bulletPooled == null)
                    {
                        Destroy(gameObject);
                    }
                    else
                    {
                        bulletPooled.SelfDestroy();
                    }

                }
            }
        }
    }
}
