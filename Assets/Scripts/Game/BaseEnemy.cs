﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Engine;
using Assets.Scripts.Settings;
using Assets.Scripts.UI;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class BaseEnemy : MonoBehaviour
    {
        [SerializeField]
        private EnemySettings settings;

        [SerializeField]
        private EnemyAnimator enemyAnimator;

        private HealthInfo enemyInfo;
        private RectTransform enemyInfoRect;
        private float speed;
        private float currentTime = 0;
        private bool isMoving;
        private float currentHp;
        private int enemyReward;
        private int currentWayPointIndex;
        private float startHp;
        private int damage;
        private float hbOffset;
        private bool isDying;
        public int Damage { get { return damage; } }

        private void Awake()
        {
            startHp = settings.HP;
            speed = settings.speed;
            enemyReward = settings.enemyPrice;
            damage = settings.damage;
            hbOffset = settings.HBOffset;
        }

        private List<Vector3> wayPoints;

        public float GetSpeed()
        {
            return speed;
        }

        public int GetEmenyReward()
        {
            return enemyReward;
        }
        public void InstantiateEnemy(GridTile start, GameObject info)
        {
            currentHp = startHp;
            enemyInfo = info.GetComponent<HealthInfo>();
            if (enemyInfo != null)
            {
                enemyInfo.UpdateHeath(currentHp);
            }
            else
            {
                Debug.LogError("No EnemyInfo spript on Object");
            }
            enemyInfoRect = enemyInfo.GetComponent<RectTransform>();
            Vector3 startPosition = start.transform.position;
            transform.position = startPosition;
            wayPoints = new List<Vector3>();
            GridTile currentTile = start;
            currentWayPointIndex = 0;
            while (currentTile != null)
            {
                Vector3 pos = currentTile.transform.position;
                wayPoints.Add(pos);
                currentTile = currentTile.NextTile;
            }
            if (wayPoints.Count > 2)
                isMoving = true;

            enemyAnimator.PlayMove();
            Vector3 direction = wayPoints[currentWayPointIndex + 1] - wayPoints[currentWayPointIndex];
            transform.rotation = Quaternion.FromToRotation(transform.forward, direction.normalized);

            SetHBPosition();
            enemyInfo.gameObject.SetActive(true);
        }

        public Vector3 GetPredictedPosition(float deltaTime)
        {
            float predictedTime = currentTime + deltaTime;
            Vector3 predictedPosition = transform.position;
            if (speed * predictedTime > 1)
            {
                float remind = speed * predictedTime;
                int i = 0;
                while (remind >= 1)
                {
                    remind -= 1;
                    if (remind >= 0 && wayPoints.Count > currentWayPointIndex + 1 + i)
                    {
                        predictedPosition = Vector3.Lerp(wayPoints[currentWayPointIndex + i], wayPoints[currentWayPointIndex + i + 1],
                                             remind);
                        i++;
                    }
                }
            }
            else
            {
                predictedPosition = Vector3.Lerp(wayPoints[currentWayPointIndex], wayPoints[currentWayPointIndex + 1],
                                        speed * predictedTime);
            }
            return predictedPosition;
        }

        private void Update()
        {
            if (GameController.instance.Freez || !GameController.instance.InGame)
                return;
            Move();
            SetHBPosition();
        }

        private void SetHBPosition()
        {
            Vector3 screenPosition = GameController.UiController.GetCanvasPosition(transform.position);
            enemyInfoRect.localPosition = screenPosition + Vector3.up * hbOffset;
        }

        private void Move()
        {
            if (isMoving)
            {
                currentTime += Time.deltaTime;
                Vector3 targetDir = wayPoints[currentWayPointIndex + 1] - transform.position;
                Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, speed * Time.deltaTime * 2, 0.0F);
                transform.rotation = Quaternion.LookRotation(newDir);
                transform.position = Vector3.Lerp(wayPoints[currentWayPointIndex], wayPoints[currentWayPointIndex + 1],
                    speed * currentTime);
                if (Mathf.Approximately(Vector3.Distance(transform.position, wayPoints[currentWayPointIndex + 1]), Mathf.Epsilon))
                {
                    currentTime = 0;
                    currentWayPointIndex++;

                    if (wayPoints.Count <= currentWayPointIndex + 1)
                    {
                        isMoving = false;
                    }

                }
            }
        }

        public void SelfDestroyEnemy()
        {
            isDying = false;
            enemyInfo.gameObject.GetComponent<PooledObject>().SelfDestroy();
            gameObject.GetComponent<PooledObject>().SelfDestroy();
            GameController.EnemyController.OnEnemyDestroy();
        }

        public void GetDamage(float damage)
        {
            if (isDying)
                return;
            enemyAnimator.PlayGetDamage();
            currentHp -= damage;
            enemyInfo.UpdateHeath(currentHp / startHp);
            enemyInfo.ShowDamage(damage);
            if (currentHp <= 0)
            {
                isMoving = false;
                isDying = true;
                StartCoroutine("Diyng");
                GameController.EnemyController.OnEnemyKill(this);
            }
        }

        private void OnTriggerEnter(Collider collider)
        {
            if (collider.tag == Constants.CoreObject)
            {

                CoreObject core = collider.gameObject.GetComponent<CoreObject>();
                if (core == null)
                {
                    Debug.LogError("No enemy script on object");
                    return;
                }
                isMoving = false;
                core.GetDamage(damage);

                StartCoroutine("Hitting");
            }
        }

        private IEnumerator Diyng()
        {
            float time = enemyAnimator.PlayDie();
            yield return new WaitForSeconds(time);
            SelfDestroyEnemy();
        }

        private IEnumerator Hitting()
        {
            float time = enemyAnimator.PlayHit();
            yield return new WaitForSeconds(time);
            SelfDestroyEnemy();
        }
    }
}
