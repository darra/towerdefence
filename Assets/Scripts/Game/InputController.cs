﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.Game
{
    public class InputController : MonoBehaviour
    {
        public event Action<RaycastHit> ClickOnTaggedObject;
       // public event Action<RaycastHit> ClickOnSomeObject;

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                if (EventSystem.current.IsPointerOverGameObject())
                    return;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    if (ClickOnTaggedObject != null)
                    {
                        ClickOnTaggedObject.Invoke(hit);
                    }

                    //if (hit.collider.tag == Constants.TileTag)
                    //{
                    //    GameController.EnemyController.SpawnEnemyInTile(hit.collider.gameObject.GetComponent<GridTile>());
                    //}
                }
            }
        }
    }
}
