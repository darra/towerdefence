﻿using System.Collections.Generic;
using Assets.Scripts.Settings;
using Assets.Scripts.UI;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class TowerController : MonoBehaviour
    {
        [SerializeField]
        private List<TowerSetting> towerSettings;

        [SerializeField] private Tower baseTowerPrefab;

        [SerializeField] private TowerUIManager uiManager;

        private void Awake()
        {
            uiManager.InitTowerUiManager(towerSettings);
        }

        public TowerUIManager GetUITowerManager()
        {
            return uiManager;
        }

        public void OnEmptyTowerPlaceClick(TowerPlaceHolder towerPlace)
        {
            uiManager.OpenBuildTowerPopup(towerPlace);
        }

        public void BuildTower(TowerSetting towerSettings, Transform towerPosition)
        {
            GameObject tower = Instantiate(baseTowerPrefab.gameObject);
            GameController.ResourceManager.UseResource(towerSettings.grades[0].cost);
            Tower towerScript = tower.GetComponent<Tower>();
            towerScript.InitTower(towerSettings);
            Vector3 position = towerPosition.position;
            position.y = 0;

            tower.transform.position = position;
        }

    }
    public enum TowerType
    {
        groundDefence,
        airDefence
    }
}
