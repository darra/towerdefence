﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Game
{
    public class EnemyFreezer : MonoBehaviour
    {
        [SerializeField] private Button freezeButton;
        [SerializeField] private Image freezeIcon;
        [SerializeField] private Text freezeTimer;
        private float freezeCDspeed = 0.01f;
        private float freezeTime = 10;

        private float currentFreezeTime;
        private bool isFreezing;
        private bool isFreezeCD;
        private void Awake()
        {
            freezeIcon.fillAmount = 0;
            freezeButton.interactable = true;
        }

        public void FreezeEnemy()
        {
            freezeIcon.fillAmount = 1;
            GameController.instance.StartFreez();
            isFreezeCD = true;
            isFreezing = true;
            freezeTimer.gameObject.SetActive(true);
            currentFreezeTime = freezeTime;
            freezeButton.interactable = false;
        }

        private void Update()
        {
            if (isFreezeCD)
            {
                freezeIcon.fillAmount -= Time.deltaTime* freezeCDspeed;
                if (freezeIcon.fillAmount <= 0)
                {
                    isFreezeCD = false;
                    freezeButton.interactable = true;
                }
            }

            if (isFreezing)
            {
                currentFreezeTime -= Time.deltaTime;
                freezeTimer.text = ((int) currentFreezeTime).ToString();
                if (currentFreezeTime <= 0)
                {
                    isFreezing = false;
                    GameController.instance.StopFreeze();
                    freezeTimer.gameObject.SetActive(false);

                }
            }
        }
    }
}
