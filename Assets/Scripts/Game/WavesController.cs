﻿using System.Collections.Generic;
using Assets.Scripts.Engine;
using Assets.Scripts.Settings;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Game
{
    public class WavesController : MonoBehaviour
    {
        [SerializeField]
        private List<WaveSettings> settings;

        [SerializeField]
        private List<GridTile> startTiles;

        private List<float> wavesStartTimes;
        private int currentWaveIndex;
        private float currentTime;
        private float generalTime;
        private int currentBunchCount;
        public float currentCooldownBunch;
        private bool generatingWaves;
        private UIController uiController;
        private EnemyController enemyController;
        private float waveCoolDown;

        private void Awake()
        {
            uiController = GameController.UiController;
            enemyController = GameController.EnemyController;
            currentWaveIndex = 0;
            currentTime = 0;
            wavesStartTimes = new List<float>();
            float previouseWaveStart = 0;
            for (int i = 0; i < settings.Count; i++)
            {
                previouseWaveStart += settings[i].cooldownWaweTime;
                wavesStartTimes.Add(previouseWaveStart);
                previouseWaveStart += settings[i].cooldownBunchTime*(settings[i].bunchCount - 1);
                generalTime = previouseWaveStart;
            }
            waveCoolDown = 0;
            uiController.InitWawesProgressSlider(generalTime, wavesStartTimes);

            //first bunch start without cooldown
            currentCooldownBunch = settings[currentWaveIndex].cooldownBunchTime;
        }

        public void StartWaves()
        {
            generatingWaves = true;
        }

        private void Update()
        {
            if(!GameController.instance.InGame)
                return;
            currentTime += Time.deltaTime;
            uiController.UpdatedeWaveSlider(currentTime/generalTime);
            if (!generatingWaves)
                return;

            if (waveCoolDown < settings[currentWaveIndex].cooldownWaweTime)
            {
                waveCoolDown += Time.deltaTime;
                return;
            }

            if (currentBunchCount < settings[currentWaveIndex].bunchCount)
            {    
                currentCooldownBunch += Time.deltaTime;
                if (currentCooldownBunch >= settings[currentWaveIndex].cooldownBunchTime)
                {
                    //Debug.LogError("bunch count " + currentBunchCount + " current wawe " + currentWaveIndex + " - " + currentTime);
                    currentBunchCount++;
                    SpawnBunchOfEnemy();
                    currentCooldownBunch = 0;
                }
            }
            //wave ends when all bunches are spawned
            if (currentBunchCount >= settings[currentWaveIndex].bunchCount)
            {
                currentWaveIndex++;
                currentBunchCount = 0;
                if (currentWaveIndex >= settings.Count)
                {
                    generatingWaves = false;
                    GameController.EnemyController.IsTheLastWaveEnded = true;
                    return;
                }
                //Debug.LogError("NextWave " + " - " + currentTime);
                //first bunch start without cooldown
                currentCooldownBunch = settings[currentWaveIndex].cooldownBunchTime;
                waveCoolDown = 0;

            }
        }

        private void SpawnBunchOfEnemy()
        {
            //Debug.LogError("Spawn bunch " + currentWaveIndex);
            int enemyCount = Random.Range(settings[currentWaveIndex].minEnemyCountInBunch, settings[currentWaveIndex].maxEnemyCountInBunch + 1);
            int enemyTypeIndex = Random.Range(0, settings[currentWaveIndex].enemyTypes.Count);
            int startTileIndex = Random.Range(0, startTiles.Count);
            enemyController.StartSpawning(enemyCount, settings[currentWaveIndex].enemyTypes[enemyTypeIndex], startTiles[startTileIndex]);
        }
    }
}
