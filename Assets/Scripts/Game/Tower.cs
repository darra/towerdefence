﻿using Assets.Scripts.Engine;
using Assets.Scripts.Settings;
using Assets.Scripts.UI;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class Tower : MonoBehaviour
    {
        [SerializeField]
        private TowerMenuButton gradeMenuButton;
        [SerializeField]
        private SphereCollider battleCollider;
        [SerializeField]
        private Bullet bulletPrefab;
        [SerializeField]
        private ObjectPool bulletPool;

        private GameObject towerModel;

        private float bulletSpeed;
        private float bulletDamage;
        private float radius;
        private float coolDown;

        private float currentTime;
        private bool canPullTrigger;
        private int currentLevel;
        private TowerController towerController;
        public TowerSetting TowerSetting { get; private set; }

        public void InitTower(TowerSetting towerSetting)
        {
            currentLevel = 0;
            TowerSetting = towerSetting;
            SetLevelParams();
            GameController.InputController.ClickOnTaggedObject += ClickOnSomeTower;
            towerController = GameController.TowerController;
            gradeMenuButton = towerController.GetUITowerManager().GetButton().GetComponent<TowerMenuButton>();
            gradeMenuButton.GetComponent<RectTransform>().localPosition =
                GameController.UiController.GetCanvasPosition(transform.position);
            gradeMenuButton.Init(TowerSetting, currentLevel + 1, false);
            gradeMenuButton.PointerDown += Upgrade;
            GameController.ResourceManager.ResourceAmountChanged += CheckIfCanGrade;
            bulletPool.CreateObjectPool(bulletPrefab.gameObject, 5);
            canPullTrigger = true;
        }

        private void CheckIfCanGrade()
        {
            if (CanGrade())
            {
                int cost = TowerSetting.grades[currentLevel + 1].cost;
                if (GameController.ResourceManager.CanUseResource(cost))
                {
                    Vector3 positionOnScreen = GameController.UiController.GetCanvasPosition(transform.position);
                    gradeMenuButton.GetComponent<RectTransform>().localPosition = positionOnScreen + Vector3.up * 20;
                    gradeMenuButton.Init(TowerSetting, currentLevel + 1, false);
                    gradeMenuButton.gameObject.SetActive(true);
                    return;
                }
            }
            if (gradeMenuButton.gameObject.activeSelf)
                gradeMenuButton.gameObject.SetActive(false);

        }

        private void InitModel()
        {
            if (towerModel != null)
                Destroy(towerModel);
            GameObject towerObject = (GameObject)Resources.Load(TowerSetting.grades[currentLevel].prefabName, typeof(GameObject));
            towerObject.tag = Constants.TowerObjectTag;
            GameObject newTowerModel = Instantiate(towerObject) as GameObject;
            newTowerModel.AddComponent<CapsuleCollider>();
            newTowerModel.transform.SetParent(transform);
            newTowerModel.transform.localPosition = Vector3.zero;
            towerModel = newTowerModel;
        }

        public int GetNextLevel()
        {
            if (currentLevel + 1 < TowerSetting.grades.Count)
                return currentLevel + 1;

            return -1;

        }
        private void OnDestroy()
        {
            GameController.InputController.ClickOnTaggedObject -= ClickOnSomeTower;
            GameController.ResourceManager.ResourceAmountChanged -= CheckIfCanGrade;
        }

        private void ClickOnSomeTower(RaycastHit hit)
        {
            if (gradeMenuButton.gameObject.activeSelf)
                gradeMenuButton.gameObject.SetActive(false);
            if (hit.collider.tag != Constants.TowerObjectTag)
                return;
            if (hit.collider.gameObject.gameObject.GetInstanceID() == towerModel.GetInstanceID())
            {
                if (CanGrade())
                {
                    Vector3 positionOnScreen = GameController.UiController.GetCanvasPosition(transform.position);
                    gradeMenuButton.GetComponent<RectTransform>().localPosition = positionOnScreen + Vector3.up * 20;

                    gradeMenuButton.Init(TowerSetting, currentLevel + 1, false);
                    gradeMenuButton.gameObject.SetActive(true);

                }
            }
        }

        private void SetLevelParams()
        {
            bulletSpeed = TowerSetting.grades[currentLevel].bulletSpeed;
            bulletDamage = TowerSetting.grades[currentLevel].damage;
            radius = TowerSetting.grades[currentLevel].radius;
            coolDown = TowerSetting.grades[currentLevel].cooldown;
            battleCollider.radius = radius;
            InitModel();
        }

        public bool CanGrade()
        {
            return currentLevel < TowerSetting.grades.Count - 1;
        }

        public void Upgrade(TowerSetting setting)
        {
            gradeMenuButton.gameObject.SetActive(false);
            currentLevel++;
            GameController.ResourceManager.UseResource(TowerSetting.grades[currentLevel].cost);
            SetLevelParams();
        }

        private void OnTriggerStay(Collider collider)
        {
            if (!canPullTrigger)
                return;
            if (collider.tag == Constants.EnemyTag)
            {
                BaseEnemy enemy = collider.gameObject.GetComponent<BaseEnemy>();
                if (enemy == null)
                {
                    Debug.LogError("No enemy script on object");
                    return;
                }
                GameObject bulletObject = bulletPool.GetObject(transform.position, false);
                Bullet bullet = bulletObject.GetComponent<Bullet>();
                bullet.InitBullet(transform.position, bulletDamage, bulletSpeed);
                bullet.TriggerBullet(collider.transform);
                currentTime = 0;
                canPullTrigger = false;
            }
        }

        private void Update()
        {
            currentTime += Time.deltaTime;
            if (currentTime >= coolDown)
            {
                canPullTrigger = true;
            }

        }
    }
}
