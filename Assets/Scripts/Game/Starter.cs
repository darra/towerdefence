﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Game
{
    public class Starter : MonoBehaviour
    {
        private void Awake()
        {
            DontDestroyOnLoad(transform);
        }

        public void StartGame()
        {
            SceneManager.LoadScene("Main", LoadSceneMode.Single);
        }

        public void OnMainMenu()
        {
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
        }

        public void OnExit()
        {
            Application.Quit();
        }
    }
}
