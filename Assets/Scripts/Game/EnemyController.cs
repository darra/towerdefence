﻿using Assets.Scripts.Engine;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemyPool enemyPool;

        private bool spawning;
        private int requiredCount;
        private int currentCount;
        private EnemyType requiredType;
        private GridTile startTile;
        private float swapnCooldown;
        private float currentTime;

        public bool IsTheLastWaveEnded { get; set; }

        public void StartSpawning(int count,EnemyType type, GridTile start)
        {
            requiredCount = count;
            requiredType = type;
            startTile = start;
            currentCount = 0;
            swapnCooldown = 0; 

            spawning = true;
        }



        private void SpawnEnemyInTile()
        {
            GameObject enemy = enemyPool.GetEnemyByType(requiredType, startTile.transform.position);
            GameObject enemyInfo = GameController.UiController.GetEnemyHealthBar(startTile.transform.position);
            BaseEnemy enemyScript = enemy.GetComponent<BaseEnemy>();
            if (enemyScript == null)
            {
                Debug.LogError("No EnemyScript " + enemy);
                enemyScript = enemy.AddComponent<BaseEnemy>();
            }
            enemyScript.InstantiateEnemy(startTile, enemyInfo);
            float speed = enemy.GetComponent<BaseEnemy>().GetSpeed();
            swapnCooldown = 1/speed;
        }

        public void OnEnemyDestroy()
        {
            if (IsTheLastWaveEnded)
            {
                //check if there are any alive enemies
                if (enemyPool.IsAnyEnemyAlive())
                {
                    GameController.instance.GameOver(true);
                }
            }
        }
        public void OnEnemyKill(BaseEnemy enemy)
        {
            GameController.ResourceManager.AddResource(enemy.GetEmenyReward());
            OnEnemyDestroy();
        }

        private void Update()
        {
            if (!spawning)
                return;
            currentTime += Time.deltaTime;
            if (currentTime >= swapnCooldown)
            {
                currentTime = 0;
                SpawnEnemyInTile();
                currentCount++;
            }
            if (currentCount >= requiredCount)
            {
                spawning = false;
            }
        }
    }

    public enum EnemyType
    {
        spider,
        skeleton,
        boss
    }
}
