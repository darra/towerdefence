﻿using System.Collections.Generic;
using Assets.Scripts.Engine;
using Assets.Scripts.Settings;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class EnemyPool : MonoBehaviour
    {
        [SerializeField] private List<EnemySettings> enemiesSettings;

        [SerializeField] private ObjectPool poolPrefab;

        private Dictionary<EnemyType, ObjectPool> poolDictionary;

        private void Awake()
        {
            poolDictionary = new Dictionary<EnemyType, ObjectPool>();

            for (int i= 0; i< enemiesSettings.Count; i++)
            {
                if (!poolDictionary.ContainsKey(enemiesSettings[i].type))
                {
                    GameObject poolObject = Instantiate(poolPrefab.gameObject);
                    gameObject.transform.localScale = Vector3.one;
                    
                    GameObject enemyResource = (GameObject) Resources.Load(enemiesSettings[i].prefabName);

                    if (enemyResource == null)
                    {
                        Debug.LogError("No enemy '" + enemiesSettings[i].prefabName + "' in Resources folder");
                        continue;
                    }
                    ObjectPool pool = poolObject.GetComponent<ObjectPool>();
                    pool.CreateObjectPool(enemyResource,enemiesSettings[i].bufferAmount);
                    poolDictionary.Add(enemiesSettings[i].type, pool);
                }
            }
        }

        public GameObject GetEnemyByType(EnemyType type,Vector3 startPosition)
        {
            if (!poolDictionary.ContainsKey(type))
            {
                Debug.LogError("No enemy pool for type " + type);
                return null;
            }
            return poolDictionary[type].GetObject(startPosition, true);
        }

        public void ReturnEnemy(EnemyType type, GameObject enemy)
        {
            poolDictionary[type].ReturnObjectToPool(enemy);
        }

        public bool IsAnyEnemyAlive()
        {
            foreach (var objectPool in poolDictionary)
            {
                if (objectPool.Value.ActiveObjectCount > 0)
                    return false;
            }

            return true;
        }
    }
}
