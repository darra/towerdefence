﻿using Assets.Scripts.Engine;
using Assets.Scripts.Game;
using UnityEngine;

public class TowerPlaceHolder : MonoBehaviour
{
    private void Start()
    {
        GameController.InputController.ClickOnTaggedObject += ClickOnSomeTowerPlace;
    }

    public void OnBuildTower()
    {
        GameController.InputController.ClickOnTaggedObject -= ClickOnSomeTowerPlace;
    }

    private void ClickOnSomeTowerPlace(RaycastHit hit)
    {

        if (hit.collider.tag == Constants.TowerPlaceTag)
        {
            if (hit.collider.gameObject.name == name)
            {
                GameController.TowerController.OnEmptyTowerPlaceClick(this);
            }
        }
    }
}
