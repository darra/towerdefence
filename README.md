#Tower Defence  Game prototype#


### Gameplay ###

Player should place tower in special place along the pathes. Each tower has the price for build and the price for upgrade. Player can earn coins by killing enemies.

There is booster for freezing enemy for some seconds.

Player wins when all waves are gone and there are no alive enemies and the core object is have some hp. Otherwise, whene core is destryed - player is defeated.

### Some feature ###

There is Tools menu in UnityEditor. 
"Generate map" invokes generation map consisting by tiles. These tiles are using for enemy path finding and for tower placeholder.
Map can be regenerated with any size(size is hardcoded at the moment).
"Make selected objects TowerPlaceholders"  - mark selected in hierarchy gameobject(gameobject should be tile of map) as towerPlaceHolder. 

All main game instances settings (tower, waves, enemies, core) can be balanced using ScriptableObject.


### Using assets ###

3D models:
 https://www.assetstore.unity3d.com/en/#!/content/22986
 https://www.assetstore.unity3d.com/en/#!/content/35635
 
 UI art, enviroments and towers are made by [Alexandr Vinogradov](https://grapemonkey.carbonmade.com/)
 
 Spent time - around 45 hours
